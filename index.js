const express = require('express');

const app = express();

let port = 3000;

app.use(express.json())

app.listen(port, () => console.log(`Server is running at localHost: ${port}`))

app.use(express.urlencoded({extended: true}))

app.get("/home", (request,response) => {
	response.send('Hi! This is the home page!')
})


// mock adata
let users = [
{
	"username":"creamy",
	"email":"iamadog@gmail.com",
	"password": "ieatpoop"
},
{
	"username":"shiro",
	"email":"iamacat@gmail.com",
	"password": "ipisseverywhere"
}
]

app.get("/users", (request,response) => {
	response.send(users)
})

app.delete("/delete-users", (request,response) => {
	for(i=0;i < users.length; i++){
		if(request.body.username === users[i].username){
			users.pop()
			
			message = `User ${request.body.username} has been deleted`

			break
		}else {
			message = `User ${request.body.username} does not exist!`
		}

	}
	response.send(message)
})